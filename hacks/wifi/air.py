#!/usr/bin/env python3


import os
import time


WIRELESS_INTERFACE = "wlan0"
TMP_CAP_DIR = "/tmp/air"
CAP_DIR = "/home/mdp/STORAGE/air"
SCAN_TIME_SECONDS = 600


def mkdir( directory ) :
    os.system( "mkdir -p %s" % ( directory ) )

def network_down() :
    os.system( "airmon-ng check kill" )

def monitor_up( WIRELESS_INTERFACE ) :
    os.system( "airmon-ng start %s" % WIRELESS_INTERFACE )

def monitor_down( WIRELESS_INTERFACE ) :
    os.system( "airmon-ng stop %s" % WIRELESS_INTERFACE )

def dump_up( WIRELESS_INTERFACE, TMP_CAP_DIR ) :
    os.system( "airodump-ng -w %s/air --berlin 0 %s &" % (
        TMP_CAP_DIR, WIRELESS_INTERFACE ) )

def dump_down() :
    os.system( "killall -s SIGINT airodump-ng" )

def network_up() :
    os.system( "systemctl start NetworkManager" )

def merge_cap( TMP_CAP_DIR, CAP_DIR ) :
    cap_files = os.listdir( TMP_CAP_DIR )
    cap_files_string = ""

    for file in cap_files :
        if file.split( "." )[ -1 ] == "cap" :
            if cap_files_string == "" :
                cap_files_string = "%s/%s" % ( TMP_CAP_DIR, file )
            else :
                cap_files_string = "%s %s/%s" % ( cap_files_string,
                                                 TMP_CAP_DIR, file )

    cap_files = os.listdir( CAP_DIR )
    cap_file = "air-%s.cap" % ( len( cap_files ) )

    os.system( "mergecap -w %s/%s %s" % ( CAP_DIR, cap_file, cap_files_string ) )


def main() :

    mkdir( TMP_CAP_DIR )

    network_down()  #  Search and destroy conflicting programs
    monitor_up( WIRELESS_INTERFACE )

    while True :
        try :
            dump_up( WIRELESS_INTERFACE, TMP_CAP_DIR )

            time.sleep( SCAN_TIME_SECONDS )

            dump_down()
        except :
            dump_down()
            break

    monitor_down( WIRELESS_INTERFACE )

    network_up()

    merge_cap( TMP_CAP_DIR, CAP_DIR )


if __name__ == "__main__" :
    main()
